"""Metadata module for managing Instances."""

PATH = "projects/{project}/zones/{zone}/instances/{instance}"

NATIVE_RESOURCE_TYPE = "compute.instances"
