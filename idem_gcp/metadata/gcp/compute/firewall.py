"""Metadata module for managing Firewalls."""

PATH = "projects/{project}/global/firewalls/{firewall}"

NATIVE_RESOURCE_TYPE = "compute.firewalls"
