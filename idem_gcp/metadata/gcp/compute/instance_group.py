"""Metadata module for managing Instance Groups."""

PATH = "projects/{project}/zones/{zone}/instanceGroups/{instanceGroup}"

NATIVE_RESOURCE_TYPE = "compute.instanceGroups"
