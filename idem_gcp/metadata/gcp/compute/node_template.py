"""Metadata module for managing Node Templates."""

PATH = "projects/{project}/regions/{region}/nodeTemplates/{nodeTemplate}"

NATIVE_RESOURCE_TYPE = "compute.node_templates"
