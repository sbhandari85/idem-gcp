import pytest

from tests.utils import generate_unique_name

PARAMETER = {
    "network_1_name": generate_unique_name("idem-test-network"),
    "network_2_name": generate_unique_name("idem-test-network"),
    "network_3_name": generate_unique_name("idem-test-network"),
    "network_1_auto_create_subnetworks": False,
    "network_2_auto_create_subnetworks": False,
    "network_3_auto_create_subnetworks": True,
    "peering_1_name": generate_unique_name("idem-test-peering"),
    "peering_2_name": generate_unique_name("idem-test-peering"),
    "subnetwork_1_ip_cidr_range": "10.0.0.0/24",
    "subnetwork_2_ip_cidr_range": "128.0.0.0/24",
}

NETWORK_SPEC = """
{network_name}:
  gcp.compute.network.present:
  - project: tango-gcp
  - name: {network_name}
  - auto_create_subnetworks: {auto_create_subnetworks}
  - peerings: []
  - routing_config:
      routing_mode: REGIONAL
  - network_firewall_policy_enforcement_order: AFTER_CLASSIC_FIREWALL
"""

NETWORK_WITH_PEERING_SPEC = """
{network_name}:
  gcp.compute.network.present:
  - project: tango-gcp
  - name: {network_name}
  - auto_create_subnetworks: {auto_create_subnetworks}
  - routing_config:
      routing_mode: REGIONAL
  - network_firewall_policy_enforcement_order: AFTER_CLASSIC_FIREWALL
  - peerings:
    - auto_create_routes: true
      exchange_subnet_routes: true
      export_custom_routes: false
      export_subnet_routes_with_public_ip: true
      import_custom_routes: false
      import_subnet_routes_with_public_ip: false
      name: {peering_name}
      network: {peering_network}
      peer_mtu: 1520
"""

SUBNETWORK_SPEC = """
{subnetwork_name}:
  gcp.compute.subnetwork.present:
  - network: projects/tango-gcp/global/networks/{network_name}
  - ip_cidr_range: {ip_cidr_range}
  - region: us-central1
  - private_ip_google_access: false
  - enable_flow_logs: false
  - private_ipv6_google_access: DISABLE_GOOGLE_ACCESS
  - purpose: PRIVATE
  - stack_type: IPV4_ONLY
"""

RESOURCE_TYPE_NETWORK = "compute.network"
RESOURCE_TYPE_SUBNETWORK = "compute.subnetwork"


@pytest.fixture(scope="function")
def network_1(hub, idem_cli):
    global PARAMETER

    # Create network
    present_state_sls = NETWORK_SPEC.format(
        **{
            "network_name": PARAMETER["network_1_name"],
            "auto_create_subnetworks": PARAMETER["network_1_auto_create_subnetworks"],
        }
    )

    network_ret = hub.tool.utils.call_present_from_sls(
        idem_cli,
        present_state_sls,
    )

    assert network_ret["result"], network_ret["comment"]
    assert network_ret.get(
        "new_state"
    ), "Network creation successful but new state is None"
    assert network_ret["new_state"].get("resource_id")
    resource_id = network_ret["new_state"]["resource_id"]
    PARAMETER["network_1_resource_id"] = resource_id

    # Add subnetwork
    present_state_sls = SUBNETWORK_SPEC.format(
        **{
            "subnetwork_name": PARAMETER["network_1_name"],
            "network_name": PARAMETER["network_1_name"],
            "ip_cidr_range": PARAMETER["subnetwork_1_ip_cidr_range"],
        }
    )

    subnetwork_ret = hub.tool.utils.call_present_from_sls(
        idem_cli,
        present_state_sls,
    )

    assert subnetwork_ret["result"], subnetwork_ret["comment"]
    subnetwork_resource_id = subnetwork_ret["new_state"]["resource_id"]

    try:
        yield network_ret["new_state"]
    finally:
        # Delete subnetwork
        ret = hub.tool.utils.call_absent(
            idem_cli,
            RESOURCE_TYPE_SUBNETWORK,
            PARAMETER["network_1_name"],
            subnetwork_resource_id,
        )
        assert ret["result"], ret["comment"]
        assert not ret.get("new_state")

        # Delete network
        ret = hub.tool.utils.call_absent(
            idem_cli,
            RESOURCE_TYPE_NETWORK,
            PARAMETER["network_1_name"],
            PARAMETER["network_1_resource_id"],
        )
        assert ret["result"], ret["comment"]
        assert not ret.get("new_state")


@pytest.fixture(scope="function")
def network_2(hub, idem_cli):
    global PARAMETER

    # Create network
    present_state_sls = NETWORK_WITH_PEERING_SPEC.format(
        **{
            "network_name": PARAMETER["network_2_name"],
            "peering_name": PARAMETER["peering_1_name"],
            "peering_network": PARAMETER["network_1_resource_id"],
            "auto_create_subnetworks": PARAMETER["network_2_auto_create_subnetworks"],
        }
    )

    network_ret = hub.tool.utils.call_present_from_sls(
        idem_cli,
        present_state_sls,
    )

    assert network_ret["result"], network_ret["comment"]
    assert network_ret.get(
        "new_state"
    ), "Network creation successful but new state is None"
    assert network_ret["new_state"].get("resource_id")
    resource_id = network_ret["new_state"]["resource_id"]
    PARAMETER["network_2_resource_id"] = resource_id

    # Add subnetwork
    present_state_sls = SUBNETWORK_SPEC.format(
        **{
            "subnetwork_name": PARAMETER["network_2_name"],
            "network_name": PARAMETER["network_2_name"],
            "ip_cidr_range": PARAMETER["subnetwork_2_ip_cidr_range"],
        }
    )

    subnetwork_ret = hub.tool.utils.call_present_from_sls(
        idem_cli,
        present_state_sls,
    )

    assert subnetwork_ret["result"], subnetwork_ret["comment"]
    subnetwork_resource_id = subnetwork_ret["new_state"]["resource_id"]

    try:
        yield network_ret["new_state"]
    finally:
        # Delete subnetwork
        ret = hub.tool.utils.call_absent(
            idem_cli,
            RESOURCE_TYPE_SUBNETWORK,
            PARAMETER["network_2_name"],
            subnetwork_resource_id,
        )
        assert ret["result"], ret["comment"]
        assert not ret.get("new_state")

        # Delete network
        ret = hub.tool.utils.call_absent(
            idem_cli,
            RESOURCE_TYPE_NETWORK,
            PARAMETER["network_2_name"],
            PARAMETER["network_2_resource_id"],
        )
        assert ret["result"], ret["comment"]
        assert not ret.get("new_state")


@pytest.fixture(scope="function")
def network_1_active_peering(hub, idem_cli, network_1, network_2):
    present_state_sls = NETWORK_WITH_PEERING_SPEC.format(
        **{
            "network_name": PARAMETER["network_1_name"],
            "peering_name": PARAMETER["peering_2_name"],
            "peering_network": PARAMETER["network_2_resource_id"],
            "auto_create_subnetworks": PARAMETER["network_1_auto_create_subnetworks"],
        }
    )

    # Update network - activate peering
    ret = hub.tool.utils.call_present_from_sls(
        idem_cli,
        present_state_sls,
    )

    assert ret["result"], ret["comment"]

    try:
        yield ret["new_state"]
    finally:
        # Update both networks to remove the peerings
        present_state_sls = NETWORK_SPEC.format(
            **{
                "network_name": PARAMETER["network_1_name"],
                "auto_create_subnetworks": PARAMETER[
                    "network_1_auto_create_subnetworks"
                ],
            }
        )

        ret = hub.tool.utils.call_present_from_sls(
            idem_cli,
            present_state_sls,
        )

        assert ret["result"], ret["comment"]

        present_state_sls = NETWORK_SPEC.format(
            **{
                "network_name": PARAMETER["network_2_name"],
                "auto_create_subnetworks": PARAMETER[
                    "network_2_auto_create_subnetworks"
                ],
            }
        )

        ret = hub.tool.utils.call_present_from_sls(
            idem_cli,
            present_state_sls,
        )

        assert ret["result"], ret["comment"]


@pytest.fixture(scope="function")
async def network_3(hub, ctx, idem_cli):
    global PARAMETER

    # Create network
    present_state_sls = NETWORK_SPEC.format(
        **{
            "network_name": PARAMETER["network_3_name"],
            "auto_create_subnetworks": PARAMETER["network_3_auto_create_subnetworks"],
        }
    )

    ret = hub.tool.utils.call_present_from_sls(
        idem_cli,
        present_state_sls,
    )

    assert ret["result"], ret["comment"]
    network_resource_id = ret["new_state"]["resource_id"]

    yield ret["new_state"]

    # Delete subnetworks
    subnetwork_list_ret = await hub.exec.gcp.compute.subnetwork.list(
        ctx=ctx, filter_=f"name eq {PARAMETER.get('network_3_name')}"
    )
    subnetwork_resource_ids = [
        subnetwork.get("resource_id") for subnetwork in subnetwork_list_ret["ret"]
    ]

    for resource_id in subnetwork_resource_ids:
        # All subnetwork have the same name - the name of the network
        ret = hub.tool.utils.call_absent(
            idem_cli, RESOURCE_TYPE_SUBNETWORK, PARAMETER["network_3_name"], resource_id
        )
        assert ret["result"], ret["comment"]
        assert not ret.get("new_state")

    # Delete network
    ret = hub.tool.utils.call_absent(
        idem_cli,
        RESOURCE_TYPE_NETWORK,
        PARAMETER["network_3_name"],
        network_resource_id,
    )
    assert ret["result"], ret["comment"]
    assert not ret.get("new_state")


@pytest.mark.asyncio
async def test_network_list_filter(hub, ctx):
    ret = await hub.exec.gcp.compute.network.list(ctx, filter_="name eq default")
    assert len(ret["ret"]) == 1
    network = ret["ret"][0]
    assert network["name"] == "default"

    ret = await hub.exec.gcp.compute.network.list(ctx, filter_="name ne default")
    for network in ret["ret"]:
        assert network["name"] != "default"


@pytest.mark.asyncio
async def test_network_get(hub, ctx):
    ret = await hub.exec.gcp.compute.network.get(ctx, name="default")
    network = ret["ret"]
    assert network["name"] == "default"

    ret = await hub.exec.gcp.compute.network.get(ctx, name="@@##$$%%^^")
    assert not ret["ret"]


@pytest.mark.asyncio
async def test_network_get_effective_firewalls(hub, ctx, idem_cli):
    ret = await hub.exec.gcp.compute.network.get(ctx, name="default")
    network = ret["ret"]

    ret = await hub.exec.gcp.compute.network.get_effective_firewalls(
        ctx,
        network=network["name"],
    )

    # The 'default' network has 3 firewalls, http, https, ssh.
    assert ret["ret"], ret["result"]
    assert not ret["comment"]


@pytest.mark.asyncio
async def test_network_list_peering_routes(hub, ctx, network_1_active_peering):
    network_peering = network_1_active_peering.get("peerings")[0]
    ret = await hub.exec.gcp.compute.network.list_peering_routes(
        ctx=ctx,
        network=network_1_active_peering["name"],
        peering_name=network_peering["name"],
        region="us-central1",
        direction="OUTGOING",
    )

    assert ret["result"], ret["comment"]
    peering_routes = ret["ret"]
    assert peering_routes
    peering_route_item = peering_routes[0]
    assert peering_route_item
    assert peering_route_item.get("nextHopRegion") == "us-central1"


async def test_network_switch_to_custom_mode(hub, ctx, network_3):
    # We want to make sure that auto_create_subnetworks is True and at the end it is set to False
    assert network_3["auto_create_subnetworks"]

    network_ret = await hub.exec.gcp.compute.network.switch_to_custom_mode(
        ctx=ctx,
        network=network_3["name"],
    )

    assert network_ret["result"], network_ret["comment"]
    assert network_ret["ret"]
    assert not network_ret["ret"]["auto_create_subnetworks"]
