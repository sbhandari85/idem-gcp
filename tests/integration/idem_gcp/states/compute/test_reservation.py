from collections import ChainMap

import pytest

from tests.utils import generate_unique_name

PARAMETRIZE = dict(argnames="__test", argvalues=[True, False], ids=["--test", "run"])
RESOURCE_TYPE_RESERVATION = "compute.reservation"
PROJECT = "tango-gcp"


@pytest.fixture(scope="module")
async def reservations(hub, ctx, idem_cli):
    reservations_count = 2
    created_reservations = []
    for i in range(reservations_count):
        created_reservations.append(await _create_reservation(hub, ctx))

    yield created_reservations

    for i in range(reservations_count):
        _delete_reservation(hub, idem_cli, created_reservations[i])


async def _create_reservation(hub, ctx):
    reservation_name = generate_unique_name("idem-test-reservation")
    zone = "us-central1-a"

    # use exec as there's no present for reservations yet
    create_ret = await hub.exec.gcp_api.client.compute.reservation.insert(
        ctx,
        project=PROJECT,
        zone=zone,
        body={
            "name": reservation_name,
            "specificReservation": {
                "count": 1,
                "instanceProperties": {"machineType": "e2-medium"},
            },
        },
    )

    assert create_ret["result"]

    operation = create_ret["ret"]
    operation_id = hub.tool.gcp.resource_prop_utils.parse_link_to_resource_id(
        operation.get("selfLink"), "compute.zone_operation"
    )
    handle_operation_ret = await hub.tool.gcp.operation_utils.handle_operation(
        ctx, {"operation_id": operation_id}, "compute.reservation", True
    )
    assert handle_operation_ret["result"]

    reservation_resource_id = handle_operation_ret["resource_id"]
    assert reservation_resource_id

    reservation_ret = await hub.exec.gcp.compute.reservation.get(
        ctx, resource_id=reservation_resource_id
    )
    assert reservation_ret["result"]

    reservation = reservation_ret["ret"]
    assert reservation
    return reservation


def _delete_reservation(hub, idem_cli, reservation):
    ret = hub.tool.utils.call_absent(
        idem_cli,
        "compute.reservation",
        reservation.get("name"),
        reservation.get("resource_id"),
    )
    assert ret["result"], ret["comment"]
    assert not ret.get("new_state")


@pytest.mark.asyncio
async def test_reservation_describe(hub, ctx, reservations):
    ret = await hub.states.gcp.compute.reservation.describe(ctx)

    assert len(ret) >= 2
    reservation = reservations[0]
    reservation_resource_id = reservation.get("resource_id")
    assert reservation_resource_id in ret

    for resource_id in ret:
        described_resource = ret[resource_id].get("gcp.compute.reservation.present")
        assert described_resource
        described_reservation = dict(ChainMap(*described_resource))
        assert described_reservation.get("resource_id") == resource_id
        if resource_id == reservation_resource_id:
            assert reservation == described_reservation


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
async def test_reservation_absent_by_resource_id(
    hub, ctx, idem_cli, reservations, __test
):
    reservation_to_delete = reservations[0]
    reservation_name = reservation_to_delete.get("name")
    reservation_resource_id = reservation_to_delete.get("resource_id")

    ret = hub.tool.utils.call_absent(
        idem_cli,
        RESOURCE_TYPE_RESERVATION,
        reservation_name,
        reservation_resource_id,
        test=__test,
        additional_kwargs=["--get-resource-only-with-resource-id"],
    )

    assert ret["result"], ret["comment"]
    assert ret["new_state"] is None
    assert ret["old_state"] == reservation_to_delete

    if __test:
        assert ret["comment"] == [
            hub.tool.gcp.comment_utils.would_delete_comment(
                resource_type=f"gcp.{RESOURCE_TYPE_RESERVATION}", name=reservation_name
            )
        ]
    else:
        assert ret["comment"] == [
            hub.tool.gcp.comment_utils.delete_comment(
                resource_type=f"gcp.{RESOURCE_TYPE_RESERVATION}", name=reservation_name
            )
        ]

    if not __test:
        # check that the resource is truly deleted
        reservation_ret = await hub.exec.gcp.compute.reservation.get(
            ctx, resource_id=reservation_resource_id
        )
        assert reservation_ret["result"], reservation_ret["comment"]
        assert not reservation_ret["ret"]


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
async def test_reservation_absent_by_properties_resource_id_flag(
    hub, ctx, idem_cli, reservations, __test
):
    reservation_to_delete = reservations[1]
    reservation_name = reservation_to_delete.get("name")

    ret = hub.tool.utils.call_absent(
        idem_cli,
        RESOURCE_TYPE_RESERVATION,
        reservation_name,
        resource_id=None,
        test=__test,
        additional_kwargs=["--get-resource-only-with-resource-id"],
        absent_params={"project": PROJECT, "zone": reservation_to_delete.get("zone")},
    )

    assert ret["result"], ret["comment"]
    assert [
        hub.tool.gcp.comment_utils.already_absent_comment(
            resource_type=f"gcp.{RESOURCE_TYPE_RESERVATION}", name=reservation_name
        )
    ] == ret["comment"]

    assert not ret.get("new_state")
    assert not ret.get("old_state")


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
async def test_reservation_absent_by_properties_no_resource_id_flag(
    hub, ctx, idem_cli, reservations, __test
):
    reservation_to_delete = reservations[1]
    reservation_name = reservation_to_delete.get("name")

    ret = hub.tool.utils.call_absent(
        idem_cli,
        RESOURCE_TYPE_RESERVATION,
        reservation_name,
        resource_id=None,
        test=__test,
        absent_params={"project": PROJECT, "zone": reservation_to_delete.get("zone")},
    )

    assert ret["result"], ret["comment"]
    assert ret["new_state"] is None
    assert ret["old_state"] == reservation_to_delete

    if __test:
        assert ret["comment"] == [
            hub.tool.gcp.comment_utils.would_delete_comment(
                resource_type=f"gcp.{RESOURCE_TYPE_RESERVATION}", name=reservation_name
            )
        ]
    else:
        assert ret["comment"] == [
            hub.tool.gcp.comment_utils.delete_comment(
                resource_type=f"gcp.{RESOURCE_TYPE_RESERVATION}", name=reservation_name
            )
        ]

    if not __test:
        # check that the resource is truly deleted
        reservation_ret = await hub.exec.gcp.compute.reservation.get(
            ctx, resource_id=reservation_to_delete.get("resource_id")
        )
        assert reservation_ret["result"], reservation_ret["comment"]
        assert not reservation_ret["ret"]
