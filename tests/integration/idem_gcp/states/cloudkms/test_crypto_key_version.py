import copy
import time

import pytest

PARAMETER = {
    "idem_name": "idem-test-crypto_key_version-" + str(int(time.time())),
    "project_id": "tango-gcp",
    "location_id": "us-east1",
    "key_ring_id": "cicd-idem-gcp-1",
    "crypto_key_id": "cicd-key-1",
    "crypto_key_version_id": "3",
}


RESOURCE_TYPE_CRYPTO_KEY_VERSION = "cloudkms.crypto_key_version"
RESOURCE_TYPE_CRYPTO_KEY_VERSION_PROPERTIES = (
    "cloudkms.projects.locations.key_rings.crypto_keys.crypto_key_versions"
)
ABSENT_STATE = {
    "name": f"{PARAMETER['idem_name']}",
    "resource_id": f"projects/{PARAMETER['project_id']}/locations/{PARAMETER['location_id']}/keyRings/{PARAMETER['key_ring_id']}/cryptoKeys/{PARAMETER['crypto_key_id']}/cryptoKeyVersions/{PARAMETER['crypto_key_version_id']}",
}

PRESENT_STATE = {
    "name": f"{PARAMETER['idem_name']}",
    "key_state": "ENABLED",
    "project_id": f"{PARAMETER['project_id']}",
    "location_id": f"{PARAMETER['location_id']}",
    "key_ring_id": f"{PARAMETER['key_ring_id']}",
    "crypto_key_id": f"{PARAMETER['crypto_key_id']}",
    "crypto_key_version_id": f"{PARAMETER['crypto_key_version_id']}",
}


PRESENT_WITH_RESOURCE_ID = {
    "resource_id": f"projects/{PARAMETER['project_id']}/locations/{PARAMETER['location_id']}/keyRings/{PARAMETER['key_ring_id']}/cryptoKeys/{PARAMETER['crypto_key_id']}/cryptoKeyVersions/{PARAMETER['crypto_key_version_id']}"
}

PARAMETRIZE = dict(argnames="__test", argvalues=[True, False], ids=["--test", "run"])


# This test does not delete the crypto key version
# but just changes the key_state to be "DESTROY_SCHEDULED"
# so within the present test to be tested the update logic.
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(name="absent")
def test_crypto_key_version_absent(hub, idem_cli, tests_dir, __test):
    global PARAMETER
    ret = hub.tool.utils.call_absent(
        idem_cli,
        RESOURCE_TYPE_CRYPTO_KEY_VERSION,
        name=PARAMETER["idem_name"],
        resource_id=PRESENT_WITH_RESOURCE_ID["resource_id"],
        test=__test,
    )

    assert ret["result"], ret["comment"]


@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(name="present", depends=["absent"])
def test_crypto_key_version_present(hub, idem_cli, tests_dir, __test):
    ret = hub.tool.utils.call_present_from_properties(
        idem_cli,
        RESOURCE_TYPE_CRYPTO_KEY_VERSION,
        PRESENT_STATE,
        __test,
    )
    assert ret["result"], ret["comment"]

    up_to_date_comment = hub.tool.gcp.comment_utils.up_to_date_comment(
        "gcp.cloudkms.crypto_key_version",
        PRESENT_WITH_RESOURCE_ID["resource_id"],
    )
    # Here the test asserts for one of these two comments (would_update/update + up_to_date)
    # because it cannot be sure that after absent test this particular test will
    # be executed prior some other present in another pipeline.
    if __test:
        would_update_comment = hub.tool.gcp.comment_utils.would_update_comment(
            "gcp.cloudkms.crypto_key_version",
            PRESENT_WITH_RESOURCE_ID["resource_id"],
        )

        assert (
            would_update_comment in ret["comment"]
            or up_to_date_comment in ret["comment"]
        )
    else:
        update_comment = hub.tool.gcp.comment_utils.update_comment(
            "gcp.cloudkms.crypto_key_version",
            PRESENT_WITH_RESOURCE_ID["resource_id"],
        )

        assert update_comment in ret["comment"] or up_to_date_comment in ret["comment"]

    assert ret["old_state"], ret["comment"]
    assert ret["new_state"], ret["comment"]


# Cannot be tested without --test flag, otherwise it
# will create a new version for the parent crypto key,
# just incrementing to the next possible version
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(
    name="present_get_resource_only_with_resource_id_missing_resource_id",
    depends=["present"],
)
def test_crypto_key_present_get_resource_only_with_resource_id_missing_resource_id(
    hub, idem_cli, tests_dir, __test
):
    if __test:
        present_state = copy.deepcopy(PRESENT_STATE)
        ret = hub.tool.utils.call_present_from_properties(
            idem_cli,
            RESOURCE_TYPE_CRYPTO_KEY_VERSION,
            present_state,
            __test,
            ["--get-resource-only-with-resource-id"],
        )

        assert ret["result"], ret["comment"]
        assert (
            hub.tool.gcp.comment_utils.would_create_comment(
                "gcp.cloudkms.crypto_key_version",
                PRESENT_WITH_RESOURCE_ID["resource_id"],
            )
            in ret["comment"]
        )


@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(
    name="present_get_resource_only_with_resource_id_resource_id", depends=["present"]
)
def test_crypto_key_version_present_get_resource_only_with_resource_id_resource_id(
    hub, idem_cli, tests_dir, __test
):
    present_state = copy.deepcopy(PRESENT_STATE)
    present_state.update(PRESENT_WITH_RESOURCE_ID)
    ret = hub.tool.utils.call_present_from_properties(
        idem_cli,
        RESOURCE_TYPE_CRYPTO_KEY_VERSION,
        present_state,
        __test,
        ["--get-resource-only-with-resource-id"],
    )

    if __test:
        assert ret["result"], ret["comment"]
    else:
        assert ret["result"], ret["comment"]
        assert (
            hub.tool.gcp.comment_utils.up_to_date_comment(
                "gcp.cloudkms.crypto_key_version",
                PRESENT_WITH_RESOURCE_ID["resource_id"],
            )
            in ret["comment"]
        )
        assert ret["old_state"], ret["comment"]
        assert not ret["new_state"], ret["comment"]


@pytest.mark.asyncio
async def test_crypto_key_version_describe(hub, ctx):
    ret = await hub.states.gcp.cloudkms.crypto_key_version.describe(ctx)
    assert len(ret) > 2
